﻿

CREATE TABLE [ROL](
	id_Rol int identity(1,1) primary key,
	nombre_Rol varchar(50) not null,
	descripcion_Rol varchar(200),
	estado_Rol bit,
);

CREATE TABLE [CLIENTE](
	id_Cliente int identity(1,1) primary key,
	primer_Nombre_Cliente varchar(50) not null,
	segundo_Nombre_Cliente varchar(50),
	primer_Apellido_Cliente varchar(50) not null,
	segundo_Apellido_Cliente varchar(50),
	correro_Electornico_Cliente varchar(200) not null,
	telefono_Cliente int not null,
	rol_Cliente int not null,
	usuario_Clientes varchar(100) not null,
	contrasena varchar(100) not null,
	CONSTRAINT FK_Rol_Cliente FOREIGN KEY (rol_Cliente) REFERENCES Rol(id_Rol)
);

CREATE TABLE [PELICULA](
	id_Pelicula int identity(1,1) primary key,
	titulo_Pelicula varchar(100) not null,
	descripcion_Pelicula varchar(500) not null,
	lista_Actores_Pelicula varchar(200) not null,
	director_Pelicula varchar(50) not null,
	costo_Alquiler_Pelicula float not null,
	cantidad_Inventario_pelicula int not null,
);

CREATE TABLE [CLIENTE_PELICULA](
	id_Reserva int identity(1,1) primary key,
	id_Pelicula int not null,
	id_Cliente int not null,
	codigo varchar(50) not null,
	descripcion varchar(200) not null,
	CONSTRAINT FK_Cliente_Pelicula FOREIGN KEY (id_Pelicula) REFERENCES Pelicula(id_Pelicula),
	CONSTRAINT FK_Pelicula_Cliete FOREIGN KEY (id_Cliente) REFERENCES Cliente(id_Cliente),
)