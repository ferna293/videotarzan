﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using VideoTarzan.Context;
using VideoTarzan.Models;

namespace VideoTarzan.Controllers
{
    //[Autenticacion(eTipoUsuario.Ninguno)]
    public class SesionController : Controller
    {
        private VideoContext db = new VideoContext();

        // GET: Sesion
        public ActionResult Iniciar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Iniciar(Sesion sesion)
        {
            if (ModelState.IsValid)
            {
                var pass = Global.Hasher.Hash(sesion.Clave);
                var user = db.cliente
                    .Where(u => u.usuario == sesion.NombreUsuario && u.contrasena == pass)
                    .FirstOrDefault();
                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(user.rolCliente.ToString(), false);
                    return this.RedirectToAction("Index", "Home", new { Area = "" });
                }
            }
            return View();
        }

        public ActionResult Salir()
        {
            FormsAuthentication.SignOut();
            return this.RedirectToAction("Index");
        }
    }
}