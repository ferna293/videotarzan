﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using VideoTarzan.Context;
using VideoTarzan.Models;

namespace VideoTarzan.Controllers
{
    [Authorize]
    public class PeliculaReservasController : Controller
    {
        private VideoContext db = new VideoContext();

        // GET: PeliculaReservas
        public ActionResult Index()
        {
            return View(db.peliculaReserva.ToList());
        }

        // GET: PeliculaReservas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliculaReserva peliculaReserva = db.peliculaReserva.Find(id);
            if (peliculaReserva == null)
            {
                return HttpNotFound();
            }
            return View(peliculaReserva);
        }

        // GET: PeliculaReservas/Create
        public ActionResult Create()
        {
            ViewBag.idCliente = new SelectList(db.cliente, "idCliente", "primerNombre");
            ViewBag.idPelicula = new SelectList(db.pelicula, "idPelicula", "titulo");
            return View();
        }

        // POST: PeliculaReservas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idReserva,idPelicula,idCliente,codigo,descripcion")] PeliculaReserva peliculaReserva)
        {
            if (ModelState.IsValid)
            {
                db.peliculaReserva.Add(peliculaReserva);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(peliculaReserva);
        }

        // GET: PeliculaReservas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliculaReserva peliculaReserva = db.peliculaReserva.Find(id);
            if (peliculaReserva == null)
            {
                return HttpNotFound();
            }
            return View(peliculaReserva);
        }

        // POST: PeliculaReservas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idReserva,idPelicula,idCliente,codigo,descripcion")] PeliculaReserva peliculaReserva)
        {
            if (ModelState.IsValid)
            {
                db.Entry(peliculaReserva).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peliculaReserva);
        }

        // GET: PeliculaReservas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliculaReserva peliculaReserva = db.peliculaReserva.Find(id);
            if (peliculaReserva == null)
            {
                return HttpNotFound();
            }
            return View(peliculaReserva);
        }

        // POST: PeliculaReservas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PeliculaReserva peliculaReserva = db.peliculaReserva.Find(id);
            db.peliculaReserva.Remove(peliculaReserva);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}