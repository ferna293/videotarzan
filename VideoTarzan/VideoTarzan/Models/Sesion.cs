﻿using System.ComponentModel.DataAnnotations;

namespace VideoTarzan.Models
{
    public class Sesion
    {
        [Display(Name = "Usuario")]
        [Required]
        public string NombreUsuario { get; set; }

        [Display(Name = "Contraseña")]
        [Required]
        [DataType(DataType.Password)]
        public string Clave { get; set; }
    }
}