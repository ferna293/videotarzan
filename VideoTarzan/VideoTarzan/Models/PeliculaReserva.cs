﻿using System.ComponentModel.DataAnnotations;

namespace VideoTarzan.Models
{
    public class PeliculaReserva
    {
        [Key]
        public int idReserva { get; set; }

        [Display(Name = "Pelicula")]
        public int idPelicula { get; set; }

        [Display(Name = "Cliente")]
        public int idCliente { get; set; }

        [Display(Name = "Codigo")]
        public string codigo { get; set; }

        [Display(Name = "Descripcion")]
        public string descripcion { get; set; }
    }
}