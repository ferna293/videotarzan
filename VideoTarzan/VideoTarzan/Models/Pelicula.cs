﻿using System.ComponentModel.DataAnnotations;

namespace VideoTarzan.Models
{
    public class Pelicula
    {
        [Key]
        public int idPelicula { get; set; }

        [Display(Name = "Titulo")]
        public string titulo { get; set; }

        [Display(Name = "Descripcion")]
        public string descripcion { get; set; }

        [Display(Name = "Lista de Actores")]
        public string listaActores { get; set; }

        [Display(Name = "Director")]
        public string director { get; set; }

        [Display(Name = "Costo del Alquiler")]
        public float costoAlquiler { get; set; }

        [Display(Name = "Cantidad de Inventario")]
        public int cantidadInventario { get; set; }
    }
}