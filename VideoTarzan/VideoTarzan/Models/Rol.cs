﻿using System.ComponentModel.DataAnnotations;

namespace VideoTarzan.Models
{
    public class Rol
    {
        [Key]
        public int idRol { get; set; }

        [Display(Name = "Nombre")]
        public string nombreRol { get; set; }

        [Display(Name = "Descripcion")]
        public string descripcion { get; set; }

        [Display(Name = "Estado")]
        public bool estado { get; set; }
    }
}