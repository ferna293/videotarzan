﻿using System.Linq;
using VideoTarzan.Context;

namespace VideoTarzan.Models
{
    public static class Permiso
    {
        private static VideoContext db = new VideoContext();

        public static bool TienePermiso()
        {
            bool per = db.cliente.Select(x => x.rolCliente == 1).FirstOrDefault();
            return per;
        }
    }
}