﻿using System.ComponentModel.DataAnnotations;

namespace VideoTarzan.Models
{
    public class Cliente
    {
        [Key]
        public int idCliente { get; set; }

        [Display(Name = "Primer Nombre")]
        public string primerNombre { get; set; }

        [Display(Name = "Segundo Nombre")]
        public string segundoNombre { get; set; }

        [Display(Name = "Primer Apellido")]
        public string primerApellido { get; set; }

        [Display(Name = "Segundo Apellido")]
        public string segundoApellido { get; set; }

        [Display(Name = "Correo Electronico")]
        public string correoElectronico { get; set; }

        [Display(Name = "Telefono")]
        public int telefono { get; set; }

        [Display(Name = "Rol")]
        public int rolCliente { get; set; }

        [Display(Name = "Usuario")]
        public string usuario { get; set; }

        [Display(Name = "Contraseña")]
        public string contrasena { get; set; }
    }
}