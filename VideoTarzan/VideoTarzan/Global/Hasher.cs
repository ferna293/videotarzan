﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace VideoTarzan.Global
{
    public class Hasher
    {
        //private static readonly string salt = "XYXY";

        public static string Hash(string pass)
        {
            string saltAndPwd = String.Concat(pass);
            UTF8Encoding encoder = new UTF8Encoding();
            SHA256Managed sha256hasher = new SHA256Managed();
            byte[] hashedDataBytes = sha256hasher.ComputeHash(encoder.GetBytes(saltAndPwd));

            return byteArrayToString(hashedDataBytes);
        }

        private static string byteArrayToString(byte[] inputArray)
        {
            StringBuilder output = new StringBuilder("");
            for (int i = 0; i < inputArray.Length; i++)
            {
                output.Append(inputArray[i].ToString("X2"));
            }
            return output.ToString();
        }

        public static bool Compare(string hash, string pass)
        {
            return hash.Equals(Hash(pass));
        }
    }
}