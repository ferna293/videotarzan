﻿using System.Data.Entity.ModelConfiguration;
using VideoTarzan.Models;

namespace VideoTarzan.Context.Mappers
{
    public class ClienteMapper : EntityTypeConfiguration<Cliente>
    {
        public ClienteMapper()
        {
            ToTable("CLIENTE", "dbo");
            HasKey(p => p.idCliente);
            Property(p => p.idCliente).HasColumnName("id_Cliente");
            Property(p => p.primerNombre).HasColumnName("primer_Nombre_Cliente").HasMaxLength(50).IsRequired();
            Property(p => p.segundoNombre).HasColumnName("segundo_Nombre_Cliente").HasMaxLength(50);
            Property(p => p.primerApellido).HasColumnName("primer_Apellido_Cliente").HasMaxLength(50).IsRequired();
            Property(p => p.segundoApellido).HasColumnName("segundo_Apellido_Cliente").HasMaxLength(50);
            Property(p => p.correoElectronico).HasColumnName("correro_Electornico_Cliente").HasMaxLength(200);
            Property(p => p.telefono).HasColumnName("telefono_Cliente");
            Property(p => p.rolCliente).HasColumnName("rol_Cliente");
            Property(p => p.usuario).HasColumnName("usuario_Clientes");
            Property(p => p.contrasena).HasColumnName("contrasena");
        }
    }
}