﻿using System.Data.Entity.ModelConfiguration;
using VideoTarzan.Models;

namespace VideoTarzan.Context.Mappers
{
    public class PeliculaMapper : EntityTypeConfiguration<Pelicula>
    {
        public PeliculaMapper()
        {
            ToTable("PELICULA", "dbo");
            HasKey(p => p.idPelicula);
            Property(p => p.idPelicula).HasColumnName("id_Pelicula");
            Property(p => p.titulo).HasColumnName("titulo_Pelicula").HasMaxLength(100).IsRequired();
            Property(p => p.descripcion).HasColumnName("descripcion_Pelicula").HasMaxLength(500).IsRequired();
            Property(p => p.listaActores).HasColumnName("lista_Actores_Pelicula").HasMaxLength(200).IsRequired();
            Property(p => p.director).HasColumnName("director_Pelicula").HasMaxLength(50).IsRequired();
            Property(p => p.costoAlquiler).HasColumnName("costo_Alquiler_Pelicula");
            Property(p => p.cantidadInventario).HasColumnName("cantidad_Inventario_pelicula");
        }
    }
}