﻿using System.Data.Entity.ModelConfiguration;
using VideoTarzan.Models;

namespace VideoTarzan.Context.Mappers
{
    public class RolMapper : EntityTypeConfiguration<Rol>
    {
        public RolMapper()
        {
            ToTable("ROL", "dbo");
            HasKey(p => p.idRol);
            Property(p => p.idRol).HasColumnName("id_Rol");
            Property(p => p.nombreRol).HasColumnName("nombre_Rol").HasMaxLength(50).IsRequired();
            Property(p => p.descripcion).HasColumnName("descripcion_Rol").HasMaxLength(200);
            Property(p => p.estado).HasColumnName("estado_Rol");
        }
    }
}