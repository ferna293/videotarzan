﻿using System.Data.Entity.ModelConfiguration;
using VideoTarzan.Models;

namespace VideoTarzan.Context.Mappers
{
    public class PeliculaReservaMapper : EntityTypeConfiguration<PeliculaReserva>
    {
        public PeliculaReservaMapper()
        {
            ToTable("CLIENTE_PELICULA", "dbo");
            HasKey(p => p.idReserva);
            Property(p => p.idReserva).HasColumnName("id_Reserva");
            Property(p => p.codigo).HasColumnName("codigo").HasMaxLength(50).IsRequired();
            Property(p => p.descripcion).HasColumnName("descripcion").HasMaxLength(200).IsRequired();
            Property(p => p.idCliente).HasColumnName("id_Cliente").IsRequired();
            Property(p => p.idPelicula).HasColumnName("id_Pelicula").IsRequired();
        }
    }
}