﻿using System.Data.Entity;
using VideoTarzan.Context.Mappers;
using VideoTarzan.Models;

namespace VideoTarzan.Context
{
    public class VideoContext : DbContext
    {
        public VideoContext() : base("DefaultConnection")
        {
        }

        public DbSet<Rol> rol { get; set; }
        public DbSet<Cliente> cliente { get; set; }
        public DbSet<Pelicula> pelicula { get; set; }
        public DbSet<PeliculaReserva> peliculaReserva { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new RolMapper());
            modelBuilder.Configurations.Add(new ClienteMapper());
            modelBuilder.Configurations.Add(new PeliculaMapper());
            modelBuilder.Configurations.Add(new PeliculaReservaMapper());
        }
    }
}